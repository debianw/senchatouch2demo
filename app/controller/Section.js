Ext.define('app.controller.Section', {
    
    extend: 'Ext.app.Controller',

    config: {

        refs: {
            section1: {
                selector: 'section1',
                xtype: 'section1',
                autoCreate: true
            }
        }

    },

    /**
     *
     */
    init: function() {
        this.getApplication().on({
            section1: this.onSection1,
            scope: this
        });
    },

    /**
     *
     */
    onSection1: function() {
        Ext
            .getCmp('viewport')
            .animateTo('left')
            .setActiveItem(
                this.getSection1()
            );
    }

});