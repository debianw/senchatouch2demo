Ext.define('app.controller.Index', {
    
    extend: 'Ext.app.Controller',

    config: {

        refs: {

            btnSection1: 'button[action=section_1]',
            homeIcon: 'homeIcon',
            dashboard: 'dashboard'

        },

        control: {
            dashboard: {
                activate: 'onActivateDashboard'
            },

            btnSection1: {
                tap: 'onSection1'
            },

            homeIcon: {
                tap: 'onHomeIcon'
            }

        }

    },

    /**
     *
     */
    launch: function() {
        console.log("Launching index");
    },

    /**
     *
     */
    onActivateDashboard: function(container, newActiveItem, oldActiveItem) {
        // destroy old panels
        if(oldActiveItem) {
            oldActiveItem.destroy();
        }
    },

    /**
     *
     */
    onShowHome: function(options) {

        Ext.getCmp('viewport')
            .animateTo(options.direction || 'left')
            .setActiveItem(this.getDashboard());
    },

    /**
     *
     */
    onSection1: function() {
        this.getApplication().fireEvent('section1');
    },

    /**
     *
     */
    onHomeIcon: function() {
        this.onShowHome({
            direction: 'right'
        });
    }

});