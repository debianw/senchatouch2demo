Ext.define("app.view.Main", {

    id: 'viewport',

    extend: 'Ext.Container',

    config: {

        fullscreen: true,

        layout: {
            type: 'card',
            
            animation: {
                easing: 'ease-in-out',
                type: 'slide',
                direction: 'left'
            }
        },

        items: [
            {
                xtype: 'dashboard'
            }
        ]
    },

    /**
     * Animate function for change direction left/right
     */
    animateTo: function(dir) {
        this.getLayout().setAnimation({
            easing: 'ease-in-out',
            type: 'slide',
            direction: dir
        });

        return this;
    }
});
