Ext.define('app.view.Dashboard', {
    
    extend: 'Ext.Panel',

    xtype: 'dashboard',

    config: {

        items: [
            {
                xtype: 'toolbar',
                title: 'Dasboard',
                docked: 'top'
            },

            {
                xtype: 'button',
                text: 'Section 1',
                action: 'section_1'
            }
        ]

    }

});