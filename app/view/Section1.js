Ext.define('app.view.Section1', {
    
    extend: 'Ext.Panel',

    xtype: 'section1',

    requires: [
        'app.view.Icon'
    ],

    config: {

        items: [
            {
                xtype: 'toolbar',
                title: 'Section 1',

                items: [
                    {
                        xtype: 'homeIcon'
                    }
                ]
            },

            {
                html: 'Content of section 1'
            }
        ]

    }

});