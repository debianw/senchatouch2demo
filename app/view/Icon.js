
"use strict";

Ext.define('app.view.Icon', {
    
    extend: 'Ext.Button',

    xtype: 'homeIcon',

    config: {
        iconCls: 'home',
        iconMask: true,
        ui: 'plain',
        action: 'showDashboard'
    }

});